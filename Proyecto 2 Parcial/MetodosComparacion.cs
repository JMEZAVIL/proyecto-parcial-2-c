﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_2_Parcial
{
    class MetodosComparacion :Query
    {
        //List<T>(IEnumerable<T>);
        public int[] ListaNoOrdenada;
        public int N;

        public void Metodoburbuja(int[] LNO)
        {
            // Variables
            int i, j;
            int temp;
            // N Pasadas
            for (i = 0; i < N; i++)
            {
                for (j = 0; j < N; j++)
                {
                    // Comparando parejas de numeros
                    if (LNO[j] > LNO[j + 1])
                    {
                        // Asignando valores ordenados
                        temp = LNO[j];
                        LNO[j] = LNO[j + 1];
                        LNO[j + 1] = temp;
                    }
                }
            }
        }
        
        public void SeleccionarRegistros()
        {
            queryConsulta("registro", REGISTROS, "");
        }



    }
}
