﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Proyecto_2_Parcial
{
    class Registros:Query
    {
        private string id_registro = "";
        private string Nombre = "";
        private string Registro = "";

        private string tabla = "";
        private string atributos_insertar = "";
        private string atributos_seleccionar = "";



        public Registros(frmProyecto f)
        {
            //this.id_alumno = id_alumno;
            this.Nombre = f.TxtNombre.Text.Trim();
            this.Registro = f.TxtRegistro.Text.Trim();

            this.tabla = REGISTROS;
            this.atributos_insertar = registro_insertar;
            this.atributos_seleccionar = registro_seleccionar;
        }

        public Registros()
        {
            tabla = REGISTROS;
            atributos_insertar = registro_insertar;
            atributos_seleccionar = registro_seleccionar;
        }

        #region VALORES
        public string valores()
        {
            return $"{abrirParentesis}" +
                   $"{c}" +
                   $"{Nombre.ToUpper().Trim()} {ccc}" +
                   $"{Registro}" +
                   $"{c}" +
                   $"{cerrarParantesis} ";
        }
        #endregion

        #region METODOS

        public bool InsertarRegistro()
        {
            return insertar(queryInsertar(tabla, atributos_insertar, valores()));
        }

        public bool cargar(string id)
        {
            try
            {
                obtenerDatos = consulta(queryConsulta(atributos_seleccionar, tabla, $"{WHERE} {id_registro_a} {igual} {c}{id}{c}"), tabla);

                id_registro = obtenerDatos.Tables[tabla].Rows[0][0].ToString().Trim();
                this.Nombre = obtenerDatos.Tables[tabla].Rows[0][1].ToString().Trim();
                this.Registro = obtenerDatos.Tables[tabla].Rows[0][2].ToString().Trim();

                return true;
            }

            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }
        #endregion

        public void llenarDataGridView(frmProyecto f)
        {
            //return consulta(queryConsulta("*",tabla,""));
            llenarDgv(f.DgvRegistros, queryConsulta("*", tabla, ""), tabla);
        }

        #region SET AND GET
        public string Nombre1
        {
            get
            {
                return Nombre;
            }

            set
            {
                Nombre = value;
            }
        }

        public string Registro1
        {
            get
            {
                return Registro;
            }

            set
            {
                Registro = value;
            }
        }
        #endregion
    }
}