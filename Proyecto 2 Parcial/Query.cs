﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto_2_Parcial
{
    class Query : BD
    {
        #region TABLAS
        public string REGISTROS = " REGISTROS ";
        #endregion

        #region PALABRAS RESERVADAS SQL
        public string INSERT_INTO = "INSERT INTO ";
        public string igual = " = ";
        public string AND = " AND ";
        public string ORDEN_BY = " ORDER BY ";
        public string ASC = " ASC";
        public string WHERE = " WHERE ";
        public string MAX_ID = "MAX";
        public string LIKE = " LIKE ";
        public string AS = " AS ";
        public string TODO = "*";

        public string abrirParentesis = "(";
        public string cerrarParantesis = ")";
        public string c = "'";
        public string cc = ",";
        public string ccc = "','";

        #endregion

        #region ALUMNO

        public string id_registro_a = "id_registro";
        public string nombre_a = "Nombre";
        public string registro_a = "Registro";

        public string registro_insertar = "(`Nombre`,`Registro`)";
        public string registro_seleccionar = "`id_registro`,`Nombre`,`Registro`";
        #endregion

        #region METODOS
        public string queryInsertar(string tabla, string atributos, string valores)
        {
            return "INSERT INTO" + tabla + atributos + "VALUES" + valores;
        }

        public string queryConsulta(string atributo, string tabla, string condicion)
        {
            return "SELECT " + atributo + " FROM " + tabla + condicion;
        }

        public string queryActualizar(string tabla, string valores, string condicion)
        {
            return "UPDATE " + tabla + " SET " + valores + condicion;
        }
        #endregion
    }
}