﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.IO;
using System.Drawing;

namespace Proyecto_2_Parcial
{
    class BD
    {
        string parametrosConexion = System.Configuration.ConfigurationManager.ConnectionStrings["db"].ConnectionString;
        SQLiteConnection conexionBD;
        SQLiteCommand queryCommand;
        public SQLiteDataAdapter adaptador;
        public DataSet obtenerDatos;

        public bool conexionAbrir()
        {
            conexionBD = new SQLiteConnection(parametrosConexion);
            ConnectionState estadoConexion;
            try
            {
                conexionBD.Open();
                estadoConexion = conexionBD.State;
                return true;
            }
            catch (SQLiteException)
            {
                return false;
            }

        }

        public bool insertar(string sqliteInsertar)
        {
            int filasafectadas = 0;
            conexionAbrir();

            queryCommand = new SQLiteCommand(sqliteInsertar, conexionBD);
            filasafectadas = queryCommand.ExecuteNonQuery();

            conexionCerrar();

            if (filasafectadas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataSet consulta(string queryConsulta, string tabla)
        {
            conexionAbrir();

            obtenerDatos = new DataSet();
            obtenerDatos.Reset();

            adaptador = new SQLiteDataAdapter(queryConsulta, conexionBD);

            adaptador.Fill(obtenerDatos, tabla);

            conexionCerrar();

            return obtenerDatos;
        }

        public void llenarListBox(System.Windows.Forms.ListBox llenarlista, string queryLlenarListView, string columna, string tabla, ref int entra)
        {
            //obtenerDatos = new DataSet();
            //obtenerDatos.Reset();
            obtenerDatos = consulta(queryLlenarListView, tabla);

            //adaptador.Fill(obtenerDatos, tabla);

            entra = 1;
            llenarlista.DataSource = obtenerDatos.Tables[tabla];
            llenarlista.DisplayMember = columna;
            //llenarCombo.SelectedIndex = -1;
            entra = 0;
        }

        private void conexionCerrar()
        {
            if (conexionBD.State == ConnectionState.Open)
            {
                conexionBD.Close();
            }
        }

        public bool actualizar(string tabla, string datos, string condicion)
        {
            conexionAbrir();

            int filasAfectadas = 0;
            string queryActualizar = "UPDATE " + tabla + " SET " + datos + " WHERE " + condicion;

            queryCommand = new SQLiteCommand(queryActualizar, conexionBD);

            filasAfectadas = queryCommand.ExecuteNonQuery();

            conexionCerrar();

            if (filasAfectadas > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void llenarDgv(System.Windows.Forms.DataGridView dgvLlenar, string query, string tabla)
        {
            //obtenerDatos = new DataSet();
            //obtenerDatos.Reset();

            obtenerDatos = consulta(query, tabla);
            //adaptador.Fill(obtenerDatos, tabla);

            dgvLlenar.DataSource = obtenerDatos.Tables[0];
        }
    }
}
