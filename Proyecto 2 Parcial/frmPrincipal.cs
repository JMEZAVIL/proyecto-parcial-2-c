﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_2_Parcial
{
    public partial class frmProyecto : Form
    {
        public List<string> listaRegistros = new List<string>();
        Registros registros = new Registros();
        int contador = 0;

        public frmProyecto()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Registros registros = new Registros(this);

            if (txtNombre.Text == "" || txtRegistro.Text == "")
            {
                MessageBox.Show("Faltan Campos Por Llenar", caption: "Advertencia", buttons: MessageBoxButtons.OK, icon: MessageBoxIcon.Error);
            }
            else
            {
                registros.InsertarRegistro();
                txtNombre.Text = "";
                txtRegistro.Text = "";
            }
        }

        private void tsbConsultaRegistros_Click(object sender, EventArgs e)
        {
            registros.llenarDataGridView(this);
        }

        private void txtRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        #region EVENTOS Metodos de Ordenamiento
        private void tsbInsercion_Click(object sender, EventArgs e)
        {
            listaRegistros.Clear();
            inicializarformulario();
            inicialzarValoresLista(DgvRegistros);
            metodoInserccion();
            lbRegistros.DataSource = listaRegistros;
            lblNumeroComparaciones.Text = contador.ToString();
        }

        private void tsbMerge_Click(object sender, EventArgs e)
        {
            listaRegistros.Clear();
            inicializarformulario();
            inicialzarValoresLista(DgvRegistros);
            metodoMergeController();
            lbRegistros.DataSource = listaRegistros;
            lblNumeroComparaciones.Text = contador.ToString();
        }

        private void tsbShell_Click(object sender, EventArgs e)
        {
            listaRegistros.Clear();
            inicializarformulario();
            inicialzarValoresLista(DgvRegistros);
            metodoShell();
            lbRegistros.DataSource = listaRegistros;
            lblNumeroComparaciones.Text = contador.ToString();
        }

        private void tsbQuick_Click(object sender, EventArgs e)
        {
            listaRegistros.Clear();
            List<int> laQueNuncaDebioHaberNacido = new List<int>();
            inicializarformulario();
            inicialzarValoresLista(DgvRegistros);

            foreach (var valor in listaRegistros)
            {
                laQueNuncaDebioHaberNacido.Add(Convert.ToInt32(valor));
            }

            laQueNuncaDebioHaberNacido.Sort();

            foreach (var valor in laQueNuncaDebioHaberNacido)
            {
                listaRegistros.Add(valor.ToString());
            }

            lbRegistros.DataSource = listaRegistros;

            //lblNumeroComparaciones.Text = contador.ToString();

            listaRegistros.Clear();
        }

        private void tsbBurbuja_Click(object sender, EventArgs e)
        {
            listaRegistros.Clear();
            inicializarformulario();
            inicialzarValoresLista(DgvRegistros);
            metodoBurbuja();
            lbRegistros.DataSource = listaRegistros;
            lblNumeroComparaciones.Text = contador.ToString();
        }

        #endregion

        #region Métodos de ordenamiento.

        private void metodoShell()
        {
            contador = 0;
            int salto = (listaRegistros.Count+1) / 2;
            int sw;
            int auxi = 0;
            int posicion = 0;
            while (salto > 0)
            {
                sw = 1;
                while (sw != 0)
                {
                    sw = 0;
                    posicion = 1;
                    while (posicion <= ((listaRegistros.Count) - salto))
                    {
                        if (Convert.ToInt32(listaRegistros[posicion - 1]) > Convert.ToInt32(listaRegistros[(posicion - 1) + salto]))
                        {
                            contador++;
                            auxi = Convert.ToInt32(listaRegistros[(posicion - 1) + salto]);
                            listaRegistros[(posicion - 1) + salto] = listaRegistros[posicion - 1];
                            listaRegistros[(posicion - 1)] = auxi.ToString();
                            sw = 1;
                        }
                        posicion++;
                    }
                }
                salto = salto / 2;
            }
        }

        private void metodoBurbuja()
        {
            string temporal = "";
            contador = 0;

            for (int i = 0; i <= listaRegistros.Count; i++)
            {
                for (int o = 0; o <= listaRegistros.Count; o++)
                {
                    //if (o != listaRegistros.Count)
                    //{
                    try
                    {
                        if (Convert.ToInt32(listaRegistros[o]) > Convert.ToInt32(listaRegistros[o + 1]))
                        {
                            temporal = listaRegistros[o];
                            listaRegistros[o] = listaRegistros[o + 1];
                            listaRegistros[o + 1] = temporal;
                            contador++;
                        }
                    }
                    catch (Exception)
                    {
                    }
                    //}
                }
            }
        }

        private void metodoInserccion()
        {
            int posicion = 0;
            string temporal = "";

            for (var i = 1; i < listaRegistros.Count; i++)
            {
                temporal = listaRegistros[i];
                posicion = i;
                try
                {

                    for (int it = 1; Convert.ToInt32(temporal) < Convert.ToInt32(listaRegistros[posicion - 1]); posicion--)
                    {
                        listaRegistros[posicion] = listaRegistros[posicion - 1];
                        contador++;
                    }
                }
                catch (Exception)
                {
                }

                listaRegistros[posicion] = temporal;
            }
        }

        private void inicializarformulario()
        {
            lbPosicionRegistro.Text = $"";
            txtBusquedaSecuencial.Text = "";
            this.gbBuscquedaSecuancial.Size = new System.Drawing.Size(153, 68);
            lblNumeroComparaciones.Text = "";
            lbRegistros.DataSource = null;
            TxtNombre.Text = "";
            txtRegistro.Text = "";
            contador = 0;
        }

        private void metodoMergeController()
        {
            mergesort(listaRegistros, 0, listaRegistros.Count);
        }

        void mergesort(List<string> lista, int p, int r)
        {
            if (p < r)
            {
                int q = (p + r) / 2;
                mergesort(lista, p, q);
                mergesort(lista, q + 1, r);
                merge(lista, p, q, r);
            }
        }
        void merge(List<string> lista, int p, int q, int r)
        {
            List<string> listaTemporal = new List<string>();
            int i = p;
            int j = q + 1;
            int k = p;
            while ((i <= q) && (j <= r))
            {
                if (Convert.ToInt32(lista[i]) < Convert.ToInt32(lista[j]))
                {
                    listaTemporal[k] = lista[i];
                    i = i + 1;
                    k = k + 1;
                }
                else
                {
                    listaTemporal[k] = lista[j];
                    j = j + 1;
                    k = k + 1;
                }
            }
            while (i <= q)
            {
                listaTemporal[k] = lista[i];
                i = i + 1;
                k = k + 1;
            }
            while (j <= r)
            {
                listaTemporal[k] = lista[j];
                j = j + 1;
                k = k + 1;
            }
            int l = p;
            while (l <= r)
            {
                lista[l] = listaTemporal[l];
                l = l + 1;
            }
        }

        #endregion

        private void inicialzarValoresLista(DataGridView dgv)
        {
            listaRegistros.Clear();
            string valor = "";

            try
            {
                for (int i = 0; i < dgv.Rows.Count; i++)
                {

                    valor = dgv.Rows[i].Cells[2].Value.ToString();

                    if (valor != "")
                    {
                        listaRegistros.Add(valor);
                    }
                }
            }
            catch (Exception)
            {
            }
        }


        /// <summary>
        /// esta mierda no sirve para nada.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBuscarRegistro_Enter(object sender, EventArgs e)
        {
            /*if (txtBuscarRegistro.Text != "")
            {
                for (int i = 0; i <= listaRegistros.Count; i++)
                {
                    if (txtBuscarRegistro.Text == listaRegistros[i])
                    {
                        lbPosicionRegistro.Text = $"Posicion: {i + 1}";
                        this.gbBuscarRegistro.Size = new System.Drawing.Size(153, 106);
                    }
                }
            }*/
        }

        private void txtBuscarRegistro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (int)Keys.Enter)
            {
                for (int i = 0; i < listaRegistros.Count; i++)
                {
                    if (txtBusquedaSecuencial.Text == listaRegistros[i])
                    {
                        lbPosicionRegistro.Text = $"Posicion: {i + 1}";
                        this.gbBuscquedaSecuancial.Size = new System.Drawing.Size(153, 106);
                    }
                }
            }
        }
    }
}
