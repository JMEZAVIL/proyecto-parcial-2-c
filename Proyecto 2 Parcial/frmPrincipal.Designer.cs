﻿using System.Windows.Forms;

namespace Proyecto_2_Parcial
{
    partial class frmProyecto
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProyecto));
            this.btnAceptar = new System.Windows.Forms.Button();
            this.tsPrincipal = new System.Windows.Forms.ToolStrip();
            this.tsbInsertarRegistro = new System.Windows.Forms.ToolStripButton();
            this.tsbConsultaRegistros = new System.Windows.Forms.ToolStripButton();
            this.tsbBurbuja = new System.Windows.Forms.ToolStripButton();
            this.tsbQuick = new System.Windows.Forms.ToolStripButton();
            this.tsbShell = new System.Windows.Forms.ToolStripButton();
            this.tsbMerge = new System.Windows.Forms.ToolStripButton();
            this.tsbInsercion = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtRegistro = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbRegistros = new System.Windows.Forms.ListBox();
            this.dgvRegistros = new System.Windows.Forms.DataGridView();
            this.N = new System.Windows.Forms.Label();
            this.lblNumeroComparaciones = new System.Windows.Forms.Label();
            this.gbBuscquedaSecuancial = new System.Windows.Forms.GroupBox();
            this.txtBusquedaSecuencial = new System.Windows.Forms.TextBox();
            this.lbPosicionRegistro = new System.Windows.Forms.Label();
            this.tsPrincipal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegistros)).BeginInit();
            this.gbBuscquedaSecuancial.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(250, 118);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 0;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // tsPrincipal
            // 
            this.tsPrincipal.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tsPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbInsertarRegistro,
            this.tsbConsultaRegistros,
            this.tsbBurbuja,
            this.tsbQuick,
            this.tsbShell,
            this.tsbMerge,
            this.tsbInsercion});
            this.tsPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tsPrincipal.Name = "tsPrincipal";
            this.tsPrincipal.Size = new System.Drawing.Size(800, 72);
            this.tsPrincipal.TabIndex = 96;
            this.tsPrincipal.Text = "toolStrip1";
            // 
            // tsbInsertarRegistro
            // 
            this.tsbInsertarRegistro.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbInsertarRegistro.Image = ((System.Drawing.Image)(resources.GetObject("tsbInsertarRegistro.Image")));
            this.tsbInsertarRegistro.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbInsertarRegistro.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInsertarRegistro.Name = "tsbInsertarRegistro";
            this.tsbInsertarRegistro.Size = new System.Drawing.Size(105, 69);
            this.tsbInsertarRegistro.Text = "Insertar Registro";
            this.tsbInsertarRegistro.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            // 
            // tsbConsultaRegistros
            // 
            this.tsbConsultaRegistros.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbConsultaRegistros.Image = ((System.Drawing.Image)(resources.GetObject("tsbConsultaRegistros.Image")));
            this.tsbConsultaRegistros.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbConsultaRegistros.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbConsultaRegistros.Name = "tsbConsultaRegistros";
            this.tsbConsultaRegistros.Size = new System.Drawing.Size(118, 69);
            this.tsbConsultaRegistros.Text = "Consultar Registros";
            this.tsbConsultaRegistros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbConsultaRegistros.Click += new System.EventHandler(this.tsbConsultaRegistros_Click);
            // 
            // tsbBurbuja
            // 
            this.tsbBurbuja.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tsbBurbuja.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbBurbuja.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbBurbuja.Image = ((System.Drawing.Image)(resources.GetObject("tsbBurbuja.Image")));
            this.tsbBurbuja.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbBurbuja.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBurbuja.Name = "tsbBurbuja";
            this.tsbBurbuja.Size = new System.Drawing.Size(54, 69);
            this.tsbBurbuja.Text = "Burbuja";
            this.tsbBurbuja.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbBurbuja.Click += new System.EventHandler(this.tsbBurbuja_Click);
            // 
            // tsbQuick
            // 
            this.tsbQuick.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tsbQuick.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbQuick.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbQuick.Image = ((System.Drawing.Image)(resources.GetObject("tsbQuick.Image")));
            this.tsbQuick.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbQuick.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbQuick.Name = "tsbQuick";
            this.tsbQuick.Size = new System.Drawing.Size(44, 69);
            this.tsbQuick.Text = "Quick";
            this.tsbQuick.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.tsbQuick.Click += new System.EventHandler(this.tsbQuick_Click);
            // 
            // tsbShell
            // 
            this.tsbShell.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tsbShell.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbShell.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbShell.Image = global::Proyecto_2_Parcial.Properties.Resources.bloggif_58fd699b6a296;
            this.tsbShell.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbShell.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbShell.Name = "tsbShell";
            this.tsbShell.Size = new System.Drawing.Size(38, 69);
            this.tsbShell.Text = "Shell";
            this.tsbShell.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbShell.Click += new System.EventHandler(this.tsbShell_Click);
            // 
            // tsbMerge
            // 
            this.tsbMerge.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tsbMerge.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbMerge.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbMerge.Image = ((System.Drawing.Image)(resources.GetObject("tsbMerge.Image")));
            this.tsbMerge.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbMerge.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbMerge.Name = "tsbMerge";
            this.tsbMerge.Size = new System.Drawing.Size(48, 69);
            this.tsbMerge.Text = "Merge";
            this.tsbMerge.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.tsbMerge.Click += new System.EventHandler(this.tsbMerge_Click);
            // 
            // tsbInsercion
            // 
            this.tsbInsercion.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tsbInsercion.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbInsercion.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tsbInsercion.Image = ((System.Drawing.Image)(resources.GetObject("tsbInsercion.Image")));
            this.tsbInsercion.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbInsercion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInsercion.Name = "tsbInsercion";
            this.tsbInsercion.Size = new System.Drawing.Size(62, 69);
            this.tsbInsercion.Text = "Insercion";
            this.tsbInsercion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInsercion.Click += new System.EventHandler(this.tsbInsercion_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 16);
            this.label1.TabIndex = 97;
            this.label1.Text = "Nombre:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 16);
            this.label2.TabIndex = 98;
            this.label2.Text = "Registro:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(89, 33);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(236, 20);
            this.txtNombre.TabIndex = 99;
            // 
            // txtRegistro
            // 
            this.txtRegistro.Location = new System.Drawing.Point(89, 67);
            this.txtRegistro.Name = "txtRegistro";
            this.txtRegistro.Size = new System.Drawing.Size(236, 20);
            this.txtRegistro.TabIndex = 100;
            this.txtRegistro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRegistro_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtRegistro);
            this.groupBox1.Controls.Add(this.btnAceptar);
            this.groupBox1.Controls.Add(this.txtNombre);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(24, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(353, 146);
            this.groupBox1.TabIndex = 101;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Insertar Registro";
            // 
            // lbRegistros
            // 
            this.lbRegistros.FormattingEnabled = true;
            this.lbRegistros.Location = new System.Drawing.Point(399, 88);
            this.lbRegistros.Name = "lbRegistros";
            this.lbRegistros.Size = new System.Drawing.Size(220, 277);
            this.lbRegistros.TabIndex = 102;
            // 
            // dgvRegistros
            // 
            this.dgvRegistros.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvRegistros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRegistros.Location = new System.Drawing.Point(24, 240);
            this.dgvRegistros.Name = "dgvRegistros";
            this.dgvRegistros.ReadOnly = true;
            this.dgvRegistros.Size = new System.Drawing.Size(353, 127);
            this.dgvRegistros.TabIndex = 103;
            // 
            // N
            // 
            this.N.AutoSize = true;
            this.N.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.N.Location = new System.Drawing.Point(19, 370);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(303, 25);
            this.N.TabIndex = 104;
            this.N.Text = "Numero de Comparaciones:";
            // 
            // lblNumeroComparaciones
            // 
            this.lblNumeroComparaciones.AutoSize = true;
            this.lblNumeroComparaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumeroComparaciones.ForeColor = System.Drawing.Color.SpringGreen;
            this.lblNumeroComparaciones.Location = new System.Drawing.Point(317, 372);
            this.lblNumeroComparaciones.Name = "lblNumeroComparaciones";
            this.lblNumeroComparaciones.Size = new System.Drawing.Size(21, 25);
            this.lblNumeroComparaciones.TabIndex = 105;
            this.lblNumeroComparaciones.Text = "*";
            // 
            // gbBuscquedaSecuancial
            // 
            this.gbBuscquedaSecuancial.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.gbBuscquedaSecuancial.Controls.Add(this.lbPosicionRegistro);
            this.gbBuscquedaSecuancial.Controls.Add(this.txtBusquedaSecuencial);
            this.gbBuscquedaSecuancial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbBuscquedaSecuancial.Location = new System.Drawing.Point(637, 88);
            this.gbBuscquedaSecuancial.Name = "gbBuscquedaSecuancial";
            this.gbBuscquedaSecuancial.Size = new System.Drawing.Size(153, 68);
            this.gbBuscquedaSecuancial.TabIndex = 106;
            this.gbBuscquedaSecuancial.TabStop = false;
            this.gbBuscquedaSecuancial.Text = "Buscaqueda secuencial";
            // 
            // txtBusquedaSecuencial
            // 
            this.txtBusquedaSecuencial.Location = new System.Drawing.Point(6, 33);
            this.txtBusquedaSecuencial.Name = "txtBusquedaSecuencial";
            this.txtBusquedaSecuencial.Size = new System.Drawing.Size(132, 21);
            this.txtBusquedaSecuencial.TabIndex = 0;
            this.txtBusquedaSecuencial.Enter += new System.EventHandler(this.txtBuscarRegistro_Enter);
            this.txtBusquedaSecuencial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuscarRegistro_KeyPress);
            // 
            // lbPosicionRegistro
            // 
            this.lbPosicionRegistro.AutoSize = true;
            this.lbPosicionRegistro.Location = new System.Drawing.Point(17, 88);
            this.lbPosicionRegistro.Name = "lbPosicionRegistro";
            this.lbPosicionRegistro.Size = new System.Drawing.Size(0, 15);
            this.lbPosicionRegistro.TabIndex = 1;
            // 
            // frmProyecto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 400);
            this.Controls.Add(this.gbBuscquedaSecuancial);
            this.Controls.Add(this.lblNumeroComparaciones);
            this.Controls.Add(this.N);
            this.Controls.Add(this.dgvRegistros);
            this.Controls.Add(this.lbRegistros);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tsPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProyecto";
            this.Text = "Formulario Proyecto";
            this.tsPrincipal.ResumeLayout(false);
            this.tsPrincipal.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegistros)).EndInit();
            this.gbBuscquedaSecuancial.ResumeLayout(false);
            this.gbBuscquedaSecuancial.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.ToolStrip tsPrincipal;
        private System.Windows.Forms.ToolStripButton tsbInsertarRegistro;
        private System.Windows.Forms.ToolStripButton tsbConsultaRegistros;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtRegistro;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox lbRegistros;
        private System.Windows.Forms.DataGridView dgvRegistros;
        private System.Windows.Forms.ToolStripButton tsbInsercion;
        private Label N;
        private Label lblNumeroComparaciones;
        private ToolStripButton tsbBurbuja;
        private ToolStripButton tsbQuick;
        private ToolStripButton tsbShell;
        private ToolStripButton tsbMerge;
        private GroupBox gbBuscquedaSecuancial;
        private TextBox txtBusquedaSecuencial;
        private Label lbPosicionRegistro;

        public TextBox TxtNombre
        {
            get
            {
                return txtNombre;
            }

            set
            {
                txtNombre = value;
            }
        }

        public TextBox TxtRegistro
        {
            get
            {
                return txtRegistro;
            }

            set
            {
                txtRegistro = value;
            }
        }

        public DataGridView DgvRegistros
        {
            get
            {
                return dgvRegistros;
            }

            set
            {
                dgvRegistros = value;
            }
        }
    }
}

